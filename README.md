# Hotline MiDOOMi

Credit for the name goes to Nexxtic, lead developer of [Selaco](https://selacogame.com/)

## What?

This is a simple GZDoom mod, I made to learn ZScript.
It's still work in progress, there is a lot of balancing to do and enemy behaviour to improve
class HMPlayer : DoomPlayer replaces DoomPlayer
{
    bool hasWeapon;
    Default
    {
        Health 1;
        Player.StartItem "HMPistol";
        Player.StartItem "HMFist";
        Player.StartItem "Clip", 8;
    }

    States
    {
		Spawn:
			PLAY A 0 Initialize;
			PLAY A -1;
			Loop;
	}
}

extends class HMPlayer
{
	void Initialize()
	{
		hasWeapon = true;
	}
}

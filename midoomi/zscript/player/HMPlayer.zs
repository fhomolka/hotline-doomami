class HMPlayer : DoomPlayer replaces DoomPlayer
{
    bool hasWeapon;
	property HasWeapon : hasWeapon;
    Default
    {
        Health 1;
		Player.MaxHealth 1;
		Player.StartItem "HMPistol";
        Player.StartItem "HMFist";
        Player.StartItem "Clip", 8;
		HMPlayer.HasWeapon true;
    }
}

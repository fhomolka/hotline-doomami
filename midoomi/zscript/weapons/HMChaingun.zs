// --------------------------------------------------------------------------
//
// HMChaingun
//
// --------------------------------------------------------------------------

class HMChaingun : HMWeapon replaces Chaingun
{
	Default
	{
		Weapon.SelectionOrder 700;
		Weapon.AmmoUse 1;
		Weapon.AmmoGive 20;
		Weapon.AmmoType "Clip";
		Inventory.PickupMessage "$GOTCHAINGUN";
		Obituary "$OB_MPCHAINGUN";
		Tag "$TAG_CHAINGUN";
	}
	States
	{
	Ready:
		CHGG A 1 A_WeaponReady;
		Loop;
	Deselect:
		CHGG A 1 A_Lower;
		Loop;
	Select:
		CHGG A 1 A_Raise;
		Loop;
	Fire:
		CHGG A 0 A_JumpIfNoAmmo("Ready");
		CHGG AB 4 A_FireCGun;
		CHGG B 0 A_ReFire;
		Goto Ready;
	AltFire:
		PISG B 6 A_ThrowWeapon("HMChaingunThrown", "HMChaingun", "Clip");
		PISG B 5 A_ReFire;
		Goto Ready;
	Flash:
		CHGF A 5 Bright A_Light1;
		Goto LightDone;
		CHGF B 5 Bright A_Light2;
		Goto LightDone;
	Spawn:
		MGUN A -1;
		Stop;
	}
}

extend class HMChaingun
{
	action void A_FireCGun()
	{
		if (player == null)
		{
			return;
		}

		Weapon weap = player.ReadyWeapon;
		if (weap != null && invoker == weap && stateinfo != null && stateinfo.mStateType == STATE_Psprite)
		{
			if (!weap.DepleteAmmo (weap.bAltFire, true, 1))
				return;

			A_StartSound ("weapons/chngun", CHAN_WEAPON);

			State flash = weap.FindState('Flash');
			if (flash != null)
			{
				// Removed most of the mess that was here in the C++ code because SetSafeFlash already does some thorough validation.
				State atk = weap.FindState('Fire');
				let psp = player.GetPSprite(PSP_WEAPON);
				if (psp)
				{
					State cur = psp.CurState;
					int theflash = atk == cur? 0:1;
					player.SetSafeFlash(weap, flash, theflash);
				}
			}
		}
		player.mo.PlayAttacking2 ();

		//GunShot (!player.refire, "BulletPuff", BulletSlope ());
		A_FireProjectile("myTracer", random(-1, 1), 0, 0, 0, FPF_NOAUTOAIM, random(-1, 1));
		A_AlertMonsters();
	}
}


class HMChaingunThrown : ThrownWeapon
{
    States
    {
        Spawn:
            MGUN A -1;
            Loop;
        Crash:
            Goto Death;
            Stop;
        XDeath:
            Goto Death;
            Stop;
        Death:
            TNT1 A 0 TW_SpawnWeapon("HMChaingun");
            stop;

    }
}

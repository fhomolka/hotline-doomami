// --------------------------------------------------------------------------
//
// HMPShotgun
//
// Modified version of the Doom Shotgun class
//
// --------------------------------------------------------------------------

class HMShotgun : HMWeapon replaces Shotgun
{
	Default
	{
		Weapon.SelectionOrder 1300;
		Weapon.AmmoUse 1;
		Weapon.AmmoGive 8;
		Weapon.AmmoType "Shell";
		Inventory.PickupMessage "$GOTSHOTGUN";
		Obituary "$OB_MPSHOTGUN";
		Tag "$TAG_SHOTGUN";
	}
	States
	{
	Ready:
		SHTG A 1 A_WeaponReady;
		Loop;
	Deselect:
		SHTG A 1 A_Lower;
		Loop;
	Select:
		SHTG A 1 A_Raise;
		Loop;
	Fire:
		SHTG A 0 A_JumpIfNoAmmo("Ready");
		SHTG A 3;
		SHTG A 7 A_FireShotgun;
		SHTG BC 5;
		SHTG D 4;
		SHTG CB 5;
		SHTG A 3;
		SHTG A 7 A_ReFire;
		Goto Ready;
	AltFire:
		PISG B 6 A_ThrowWeapon("HMShotgunThrown", "HMShotgun", "Shell");
		PISG B 5 A_ReFire;
		Goto Ready;
	Flash:
		SHTF A 4 Bright A_Light1;
		SHTF B 3 Bright A_Light2;
		Goto LightDone;
	Spawn:
		SHOT A -1;
		Stop;
	}
}

extend class HMShotgun
{

	action void A_FireShotgun()
	{
		if (player == null)
		{
			return;
		}

		A_StartSound ("weapons/shotgf", CHAN_WEAPON);
		Weapon weap = player.ReadyWeapon;
		if (weap != null && invoker == weap && stateinfo != null && stateinfo.mStateType == STATE_Psprite)
		{
			if (!weap.DepleteAmmo (weap.bAltFire, true, 1))
				return;

			player.SetPsprite(PSP_FLASH, weap.FindState('Flash'), true);
		}
		player.mo.PlayAttacking2 ();

		double pitch = BulletSlope ();

		for (int i = 0; i < 7; i++)
		{
			//GunShot (false, "BulletPuff", pitch);
            A_FireProjectile("myTracer", random(-1,1) * i, 0, 0, 0, FPF_NOAUTOAIM, random(-1,1));
		}
		A_AlertMonsters();
	}
}

class HMShotgunThrown : ThrownWeapon
{
    States
    {
        Spawn:
            SHOT A -1;
            Loop;
        Crash:
            Goto Death;
            Stop;
        XDeath:
            Goto Death;
            Stop;
        Death:
            TNT1 A 0 TW_SpawnWeapon("HMShotgun");
            stop;

    }
}



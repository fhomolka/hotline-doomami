// --------------------------------------------------------------------------
//
// HMPistol
//
// Modified version of the Doom Pistol class
//
// --------------------------------------------------------------------------

class HMPistol : HMWeapon replaces Pistol
{
	//TODO(fhomolka): Check why AmmoGive is halved by enemy drops
	Default
	{
		Weapon.SelectionOrder 1900;
		Weapon.AmmoUse 1;
		Weapon.AmmoGive 16;
		Weapon.AmmoType "Clip";
		Weapon.AmmoType2 "Clip";
		Obituary "$OB_MPPISTOL";
		Inventory.Pickupmessage "$PICKUP_PISTOL_DROPPED";
		Tag "$TAG_PISTOL";
	}
	States
	{
	Ready:
		PISG A 1 A_WeaponReady;
		Loop;
	Deselect:
		PISG A 1 A_Lower;
		Loop;
	Select:
		PISG A 1 A_Raise;
		Loop;
	Fire:
		PISG A 0 A_JumpIfNoAmmo("Ready");
		PISG A 4;
		PISG B 6 A_FirePistol;
		PISG C 4;
		PISG B 5 A_ReFire;
		Goto Ready;
    AltFire:
		PISG B 6 A_ThrowWeapon("HMPistolThrown", "HMPistol", "Clip");
		PISG B 5 A_ReFire;
		Goto Ready;
	Flash:
		PISF A 7 Bright A_Light1;
		Goto LightDone;
		PISF A 7 Bright A_Light1;
		Goto LightDone;
 	Spawn:
		PIST A -1;
		Stop;
	}
}

extend class HMPistol
{
	action void A_FirePistol()
	{
		bool accurate;

		if (player != null)
		{
			Weapon weap = player.ReadyWeapon;
			if (weap != null && invoker == weap && stateinfo != null && stateinfo.mStateType == STATE_Psprite)
			{
				if (!weap.DepleteAmmo (weap.bAltFire, true, 1))
					return;

				player.SetPsprite(PSP_FLASH, weap.FindState('Flash'), true);
			}
			player.mo.PlayAttacking2 ();

			accurate = !player.refire;
		}
		else
		{
			accurate = true;
		}

		A_StartSound ("weapons/pistol", CHAN_WEAPON);
		//GunShot (accurate, "BulletPuff", BulletSlope ());
        A_FireProjectile("myTracer", random(-1,1), 0, 0, 0, 0, 0);
        A_AlertMonsters();
	}
}

class HMPistolThrown : ThrownWeapon
{
    States
    {
        Spawn:
            PIST A -1;
            Loop;
        Crash:
            Goto Death;
            Stop;
        XDeath:
            Goto Death;
            Stop;
        Death:
            TNT1 A 0 TW_SpawnWeapon("HMPistol");
            stop;

    }
}

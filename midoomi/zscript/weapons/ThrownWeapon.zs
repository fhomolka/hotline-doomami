/*
 * HACK(fhomolka):
 * This used to inherit from FastProjectile, but then it isn't affected by gravity
 * If the height and radius are too small (less than 2), it causes issues with clipping
 * If the speed is greater than 60, it causes issues
 * Sadly, I am limited by the technology of my era
 */
class ThrownWeapon : actor
{
    int LoadedAmmo;

    void TW_SpawnWeapon(string weap_name)
    {
        actor weap;
        bool success;
        [success, weap] = A_SpawnItemEx(weap_name);
        if(weap)
        {
            Weapon p_weap = Weapon(weap);
            // TODO(fhomolka):
            // On the lowest skill, all ammo is doubled, remember to change that
            p_weap.AmmoGive1 = LoadedAmmo;
        }
    }

    Default
    {
        PROJECTILE;
        height 3;
        radius 3;
        damage 1;
        speed 60;
        alpha 1;
        mass 100;
        gravity 2;
        -NOGRAVITY;
    }

    States
    {
        Spawn:
            TNT1 A -1;
            Loop;
        Crash:
            //TNT1 A -1 A_SpawnItem("Pistol");
            TNT1 A -1 A_Jump(256, "Null");
            Stop;
        XDeath:
            //TNT1 A -1 A_SpawnItem("Pistol");
            TNT1 A -1 A_Jump(256, "Null");
            Stop;
        Death:
            //TNT1 A -1 A_SpawnItem("Pistol");
            TNT1 A -1 A_Jump(256, "Null");
            stop;

    }
}

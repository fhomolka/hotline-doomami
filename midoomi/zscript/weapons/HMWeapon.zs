//HMWeapon
class HMWeapon : DoomWeapon replaces DoomWeapon
{
	Default
	{
		+Weapon.NoAlert;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL
	}
}

extend class HMWeapon
{
	//===========================================================================
	//
    // Original taken from gzdoom.pk3/zscript/actors/inventory.zs
    //
	// HMWeapon :: CallTryPickup
	//
	//===========================================================================

    bool, Actor CallTryPickup(Actor toucher)
	{
		let saved_toucher = toucher;
		let Invstack = Inv; // A pointer of the inventories item stack.

		// unmorphed versions of a currently morphed actor cannot pick up anything.
		if (bUnmorphed) return false, null;

		bool res;
		if (CanPickup(toucher))
		{
			res = TryPickup(toucher);
		}
		else if (!bRestrictAbsolutely)
		{
			// let an item decide for itself how it will handle this
			res = TryPickupRestricted(toucher);
		}
		else
			return false, null;

		if (!res && (bAlwaysPickup) && !ShouldStay())
		{
			res = true;
			GoAwayAndDie();
		}

		if (res)
		{
			GiveQuestItem(toucher);

			// Transfer all inventory across that the old object had, if requested.
			if (bTransfer)
			{
				while (Invstack)
				{
					let titem = Invstack;
					Invstack = titem.Inv;
					if (titem.Owner == self)
					{
						if (!titem.CallTryPickup(toucher)) // The object no longer can exist
						{
							titem.Destroy();
						}
					}
				}
			}

			// So we don't pick up any other weapons
			HMPlayer pToucher = HMPlayer(toucher);
			if(ptoucher)
			{
				pToucher.hasWeapon = true;
			}
		}
		return res, toucher;
	}

    //===========================================================================
	//
    // Original taken from gzdoom.pk3/zscript/actors/inventory.zs
    //
	// HMWeapon :: CanPickup
	//
	//===========================================================================

	override bool CanPickup(Actor toucher)
	{
		if (toucher == null) return false;

		int rsize = RestrictedToPlayerClass.Size();
		if (rsize > 0)
		{
			for (int i=0; i < rsize; i++)
			{
				if (toucher is RestrictedToPlayerClass[i]) return true;
			}
			return false;
		}
		rsize = ForbiddenToPlayerClass.Size();
		if (rsize > 0)
		{
			for (int i=0; i < rsize; i++)
			{
				if (toucher is ForbiddenToPlayerClass[i]) return false;
			}
		}
		return true;
	}

	//===========================================================================
	//
    // Original taken from gzdoom.pk3/zscript/actors/actor.zs
    //
	// HMWeapon :: CanPickup
	//
	//===========================================================================

	override void Touch (Actor toucher)
	{
		let player = toucher.player;

		// If a voodoo doll touches something, pretend the real player touched it instead.
		if (player != NULL)
		{
			toucher = player.mo;
		}

		bool localview = toucher.CheckLocalView();

		if (!toucher.CanTouchItem(self))
			return;

		//Check if the player already has a weapon, don't want to pick up another if they do
        HMPlayer pToucher = HMPlayer(toucher);
		if(ptoucher != null)
		{
			if(ptoucher.hasWeapon)
			{
				return;
			}
        }

		bool res;
		[res, toucher] = CallTryPickup(toucher);
		if (!res) return;

		// This is the only situation when a pickup flash should ever play.
		if (PickupFlash != NULL && !ShouldStay())
		{
			Spawn(PickupFlash, Pos, ALLOW_REPLACE);
		}

		if (!bQuiet)
		{
			PrintPickupMessage(localview, PickupMessage ());

			// Special check so voodoo dolls picking up items cause the
			// real player to make noise.
			if (player != NULL)
			{
				PlayPickupSound (player.mo);
				if (!bNoScreenFlash && player.playerstate != PST_DEAD)
				{
					player.bonuscount = BONUSADD;
				}
			}
			else
			{
				PlayPickupSound (toucher);
			}
		}

		// [RH] Execute an attached special (if any)
		DoPickupSpecial (toucher);

		if (bCountItem)
		{
			if (player != NULL)
			{
				player.itemcount++;
			}
			level.found_items++;
		}

		if (bCountSecret)
		{
			Actor ac = player != NULL? Actor(player.mo) : toucher;
			ac.GiveSecret(true, true);
		}

		//Added by MC: Check if item taken was the roam destination of any bot
		for (int i = 0; i < MAXPLAYERS; i++)
		{
			if (players[i].Bot != NULL && self == players[i].Bot.dest)
				players[i].Bot.dest = NULL;
		}
	}

	action void A_ThrowWeapon(string projectileName, string weaponName, string ammoName)
	{
		if (player == null) return;

		HMPlayer dPlayer = HMPlayer(player.mo);
		dPlayer.hasWeapon = false;

		let projectile = ThrownWeapon(A_FireProjectile(projectileName, random(-1,1), 0, 0, 0, 0, 0));
		if(projectile)
		{
			projectile.LoadedAmmo = CountInv(ammoName);
		}

		A_TakeInventory(weaponName, 1);
		A_TakeInventory(ammoName, 200);
	}
}

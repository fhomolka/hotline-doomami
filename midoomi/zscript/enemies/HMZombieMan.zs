class HMZombieMan : ZombieMan replaces ZombieMan
{
    Default
    {
        Health 1;
		DropItem "HMPistol";
    }

    States
    {
        Missile:
		POSS E 10 A_FaceTarget;
		POSS F 8 A_PosAttack;
		POSS E 8;
		Goto See;
    }
}

extend class HMZombieMan
{
    void A_PosAttack()
	{
		if (target)
		{
			A_FaceTarget();
			double ang = angle;
			double slope = AimLineAttack(ang, MISSILERANGE);
			A_StartSound("grunt/attack", CHAN_WEAPON);
			ang  += Random2[PosAttack]() * (22.5/256);
			int damage = Random[PosAttack](1, 5) * 3;
			//LineAttack(ang, MISSILERANGE, slope, damage, "Hitscan", "Bulletpuff");
            //A_FireProjectile("myTracer", random(-1,1), 0, -1, 0, 0, 0);
            SpawnMissile(target, "myTracer");
		}
	}
}

class HMChaingunGuy : ChaingunGuy replaces ChaingunGuy
{
    Default
    {
        Health 1;
		DropItem "HMChaingun";
    }

    States
    {
        Missile:
		CPOS E 10 A_FaceTarget;
		CPOS FE 4 BRIGHT A_CPosAttack;
		CPOS F 1 A_CPosRefire;
		Goto Missile+1;
    }
}

extend class HMChaingunGuy
{
    void A_CPosAttack()
	{
		if (target)
		{
			if (bStealth) visdir = 1;
			A_StartSound(AttackSound, CHAN_WEAPON);
			A_FaceTarget();
			double slope = AimLineAttack(angle, MISSILERANGE);
			double ang = angle + Random2[CPosAttack]() * (22.5/256);
			int damage = Random[CPosAttack](1, 5) * 3;
			//LineAttack(ang, MISSILERANGE, slope, damage, "Hitscan", "Bulletpuff");
            SpawnMissile(target, "myTracer");
		}
	}
}

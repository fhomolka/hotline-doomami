class HMPinky : Demon replaces Demon
{
	Default
	{
		Health 1;
	}
}

class HMSpectre : HMPinky replaces Spectre
{
	Default
	{
		+SHADOW
		RenderStyle "OptFuzzy";
		Alpha 0.5;
		SeeSound "spectre/sight";
		AttackSound "spectre/melee";
		PainSound "spectre/pain";
		DeathSound "spectre/death";
		ActiveSound "spectre/active";
		Obituary "$OB_SPECTREHIT";
		Tag "$FN_SPECTRE";
	}
}

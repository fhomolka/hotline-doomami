class DMShotgunGuy : ShotgunGuy replaces ShotgunGuy
{
    Default
    {
        Health 1;
		DropItem "HMShotgun";
    }

    States
    {
        Missile:
		SPOS E 10 A_FaceTarget;
		SPOS F 10 BRIGHT A_SposAttackUseAtkSound;
		SPOS E 10;
		Goto See;
    }
}

extend class DMShotgunGuy
{
    private void A_SPosAttackInternal()
	{
		if (target)
		{
			A_FaceTarget();
			double bangle = angle;
			double slope = AimLineAttack(bangle, MISSILERANGE);

			for (int i=0 ; i<3 ; i++)
			{
				//double ang = bangle + Random2[SPosAttack]() * (22.5/256);
				//int damage = Random[SPosAttack](1, 5) * 3;
				//LineAttack(ang, MISSILERANGE, slope, damage, "Hitscan", "Bulletpuff");
                SpawnMissile(target, "myTracer");
			}
		}
    }

	void A_SPosAttackUseAtkSound()
	{
		if (target)
		{
			A_StartSound(AttackSound, CHAN_WEAPON);
			A_SPosAttackInternal();
		}
	}
}

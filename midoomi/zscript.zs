version "4.6.0"

// Thirdparty stuff
#include "zscript/thirdparty/Tracer.zs"

#include "zscript/Tracers.zs"

//Gun Reworks
#include "zscript/weapons/ThrownWeapon.zs"
#include "zscript/weapons/HMWeapon.zs"
#include "zscript/weapons/HMFist.zs"
#include "zscript/weapons/HMChainsaw.zs"
#include "zscript/weapons/HMPistol.zs"
#include "zscript/weapons/HMshotgun.zs"
#include "zscript/weapons/HMChaingun.zs"
#include "zscript/weapons/HMRocketLauncher.zs"
#include "zscript/weapons/HMPlasmaRifle.zs"
#include "zscript/weapons/HMBFG9000.zs"
#include "zscript/weapons/HMSuperShotgun.zs"

#include "zscript/player/HMPlayer.zs"

//Enemy Reworks
#include "zscript/enemies/HMZombieMan.zs"
#include "zscript/enemies/HMShotgunGuy.zs"
#include "zscript/enemies/HMChaingunGuy.zs"

#include "zscript/enemies/HMImp.zs"
#include "zscript/enemies/HMPinky.zs"
#include "zscript/enemies/HMArachnotron.zs"
#include "zscript/enemies/HMArchvile.zs"
#include "zscript/enemies/HMBaronOfHell.zs"
#include "zscript/enemies/HMHellKnight.zs"
#include "zscript/enemies/HMCacodemon.zs"
#include "zscript/enemies/HMCyberdemon.zs"
#include "zscript/enemies/HMMancubus.zs"
#include "zscript/enemies/HMLostSoul.zs"
#include "zscript/enemies/HMPainElemental.zs"
#include "zscript/enemies/HMRevenant.zs"
#include "zscript/enemies/HMSpiderMastermind.zs"

//UI
#include "zscript/ui/statusbar/HMSBar.zs"
